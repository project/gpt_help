/**
 * @file
 * Attaches function for drupal ajax submit callback handle.
 */
(function($, Drupal) {
  $.fn.extend({
    setCkeditorData: function(key, content) {
      CKEDITOR.instances[key].setData(content);
    },
  });
})(jQuery, Drupal);
