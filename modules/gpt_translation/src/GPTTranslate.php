<?php

namespace Drupal\gpt_translation;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\gpt_help\GPTApiService;
use GuzzleHttp\Exception\GuzzleException;

/**
 * All data-related functions.
 */
class GPTTranslate {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * GPT api service.
   *
   * @var \Drupal\gpt_help\GPTApiService
   */
  protected $GPTApiService;

  /**
   * Configuration constructor.
   *
   * @param \Drupal\gpt_help\GPTApiService $config_factory
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(GPTApiService $GPTApiService, ConfigFactory $config_factory) {
    $this->GPTApiService = $GPTApiService;
    $this->config = $config_factory->get('gpt_translation.settings');
  }

  /**
   * GPT translate content.
   */
  public function gptTranslateContent($input_text, $lang_name) {
    $debug = $this->config->get('debug');
    if ($debug) {
      return $input_text . '+1';
    }

    $proper_nouns_config = $this->config->get('proper_nouns');
    $proper_nouns = [];
    if ($proper_nouns_config && $proper_nouns = Json::decode($proper_nouns_config, TRUE)) {
      $proper_nouns_keys = array_keys($proper_nouns);
      foreach ($proper_nouns_keys as $i => $proper_nouns_key) {
        $placeholder = '{{{proper_noun' . ($i + 1) . '}}}';
        $input_text = str_replace($proper_nouns_key, $placeholder, $input_text);
      }
    }

    $message = [
      [
        "role" => "system",
        "content" => 'You are a helpful translation assistant.',
      ],
      [
        "role" => "user",
        "content" => "Can you please help me translate the following HTML content into $lang_name? Exclude translation of proper nouns which wrapped with '{{}}}'.",
      ],
      [
        "role" => "user",
        "content" => $input_text,
      ],
    ];

    // Calling our custom GPT API service.
    try {
      $response = $this->GPTApiService->getGptResponse('', $message);
    } catch (GuzzleException $exception) {
      // Error handling for ChatGPT API call.
      $error_msg = $exception->getMessage();
      return $error_msg;
    }

    $replace_i = 0;
    foreach ($proper_nouns as $proper_noun_value) {
      $placeholder = '{{{proper_noun' . ($replace_i + 1) . '}}}';
      $response = str_replace($placeholder, $proper_noun_value, $response);
      $replace_i++;
    }
    return $response;
  }

  /**
   * Handle content in dom.
   *
   * @param $dom
   * @param $lang_name
   *
   * @return mixed
   */
  public function handleContentInDom($dom, $lang_name, $action = NULL) {
    foreach ($dom->childNodes as $node) {
      if ($node->nodeName == '#text') {
        if (preg_match("/^\d+$/", $node->nodeValue)) {
          continue;
        }
        $node->nodeValue = empty($action) ? $this->gptTranslateContent($node->nodeValue, $lang_name) : $this->gptOptimizeContent($node->nodeValue, $action);
      }
      if ($node->hasChildNodes() && $node->nodeName != 'sup') {
        $this->handleContentInDom($node, $lang_name);
      }
    }

    return $dom;
  }

  /**
   * Handle full html field.
   *
   * @param $dom
   * @param $lang_name
   *
   * @return string
   */
  public function handleFullHtmlField($dom, $lang_name, $action = NULL) {
    $xpath = new \DOMXPath($dom);
    $textNodes = $xpath->query('//text()');

    if (count($textNodes) == 1) {
      $translated_dom = $this->handleContentInDom($dom, $lang_name, $action);
      return $translated_dom->saveHTML();
    }

    $textPlaceholders = [];

    foreach ($textNodes as $node) {
      if ($node->nodeName == '#text') {
        $text = $node->nodeValue;
        if (empty($text) || $text == "\u{00A0}" || preg_match("/^\d+$/", $text)) {
          continue;
        }
        $placeholder = '[[[' . count($textPlaceholders) . ']]]';
        $textPlaceholders[$placeholder] = $text;
        $node->nodeValue = $placeholder;
      }
    }

    $placeholders_content = $dom->saveHTML();
    $cleanContent = Json::encode($textPlaceholders);
    $response_array = [];
    $strlen = strlen($cleanContent);
    $chunk_length = $this->config->get('chunk_length') ?? 1000;
    $slice_count = ceil($strlen / $chunk_length);
    if ($slice_count == 1) {
      $array_chunk = [$textPlaceholders];
    }
    else {
      $array_chunk_length = floor(count($textPlaceholders) / $slice_count);
      $array_chunk = array_chunk($textPlaceholders, $array_chunk_length, TRUE);
    }

    foreach ($array_chunk as $item) {
      try {
        $response_array = array_merge($response_array, $this->translateFullHtmlFieldInChunk($item, $lang_name));
      } catch (GuzzleException $exception) {
        // Error handling for ChatGPT API call.
        return $exception->getMessage();
      }
    }

    if (empty($response_array)) {
      return 'Some thing error for get response from gpt.';
    }
    return strtr($placeholders_content, $response_array);
  }

  /**
   * Translate full html field in chunk.
   *
   * @return array
   */
  public function translateFullHtmlFieldInChunk($textPlaceholders, $lang_name) {
    $cleanContent = Json::encode($textPlaceholders);
    $proper_nouns_config = $this->config->get('proper_nouns');
    $proper_nouns = [];
    if ($proper_nouns_config && $proper_nouns = Json::decode($proper_nouns_config, TRUE)) {
      $proper_nouns_keys = array_keys($proper_nouns);
      foreach ($proper_nouns_keys as $i => $proper_nouns_key) {
        $placeholder = '{{{proper_noun' . ($i + 1) . '}}}';
        $cleanContent = str_replace($proper_nouns_key, $placeholder, $cleanContent);
      }
    }

    $message = [
      [
        "role" => "system",
        "content" => "You are a helpful translation assistant.
        Follow these steps to translate the text.\n
        1. The content is a json string need to be decode first.\n
        2. Translate the value, context is other value, ignore the key. Exclude translation of proper nouns which wrapped with '{{}}}'.\n
        3. The response should be formatted as JSON with keys which decode from first step.\n",
      ],
      [
        "role" => "user",
        "content" => "Can you please help me translate the following HTML content into $lang_name?",
      ],
      [
        "role" => "user",
        "content" => $cleanContent,
      ],
    ];

    $result = [];
    if ($this->config->get('debug')) {
      $result = $textPlaceholders;
    }
    else {
      try {
        $response = $this->GPTApiService->getGptResponse('', $message);
      } catch (GuzzleException $exception) {
        throw $exception;
      }
      $response_array = Json::decode($response);
      if (empty($response_array)) {
        return [];
      }

      foreach ($response_array as $key => $value) {
        $replace_i = 0;
        foreach ($proper_nouns as $proper_noun_value) {
          $placeholder = '{{{proper_noun' . ($replace_i + 1) . '}}}';
          $value = str_replace($placeholder, $proper_noun_value, $value);
          $replace_i++;
        }
        $result[$key] = $value;
      }
    }
    return $result;
  }

  /**
   * GPT translate content.
   */
  public function gptSeo($input_text) {
    if ($this->config->get('debug')) {
      return Json::encode([
        'title' => 'title',
        'description' => 'description',
        'keywords' => 'keywords',
      ]);
    }

    $prompt_text = "To extract SEO title, description, and keywords information from text, follow these steps:\n\n1. Identify the main topic or focus of the text. 2. Choose a relevant and concise title that includes the main topic or focus. 3. Write a brief description of the content that summarizes its key points and uses relevant keywords. 4. Select relevant keywords that reflect the content's main topics and themes. The response should be formatted as JSON with keys as 'title', 'description', and 'keywords'" . " ``` " . $input_text . " ``` ";

    // Calling our custom GPT API service.
    try {
      $response = $this->GPTApiService->getGptResponse($prompt_text);
    } catch (GuzzleException $exception) {
      // Error handling for ChatGPT API call.
      $error_msg = $exception->getMessage();
      return $error_msg;
    }
    return $response;
  }

  /**
   * GPT optimize content.
   */
  public function gptOptimizeContent($input_text, $action, $full_html = FALSE) {
    $debug = $this->config->get('debug');
    if ($debug) {
      return $input_text . '+1';
    }

    $system_prompt_text = $this->config->get($action . '_system_prompt_text');
    $user_prompt_text = $this->config->get($action . '_user_prompt_text');
    $full_html_user_prompt_text = $this->config->get($action . '_full_html_user_prompt_text');
    if (empty($system_prompt_text) || empty($user_prompt_text) || empty($full_html_user_prompt_text)) {
      return 'Please config the GPT translation configuration';
    }

    if ($full_html) {
      $user_prompt_text = $full_html_user_prompt_text;
    }

    $message = [
      [
        "role" => "system",
        "content" => $system_prompt_text,
      ],
      [
        "role" => "user",
        "content" => $user_prompt_text,
      ],
      [
        "role" => "user",
        "content" => $input_text,
      ],
    ];

    // Calling our custom GPT API service.
    try {
      $response = $this->GPTApiService->getGptResponse('', $message);
    } catch (GuzzleException $exception) {
      // Error handling for ChatGPT API call.
      $error_msg = $exception->getMessage();
      return $error_msg;
    }

    if (strpos($response, 'Unknown') !== FALSE) {
      $response = $input_text;
    }

    return $response;
  }

  /**
   * optimize full html field.
   *
   * @param $dom
   * @param $lang_name
   *
   * @return string
   */
  public function optimizeFullHtmlField($dom, $action = NULL) {
    $xpath = new \DOMXPath($dom);
    $textNodes = $xpath->query('//text()');

    foreach ($textNodes as $node) {
      if ($node->nodeName == '#text') {
        $text = $node->nodeValue;
        if (empty($text) || $text ==   || preg_match("/^\d+$/", $text)) {
          continue;
        }
        $node->nodeValue = $this->gptOptimizeContent($text, $action);
      }
    }

    return $dom->saveHTML();
  }

}
