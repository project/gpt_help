<?php

namespace Drupal\gpt_translation;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for source plugin controllers.
 */
interface SourcePluginInterface extends PluginInspectionInterface {

  /**
   * Returns an array with the data structured for translation.
   *
   * @param \Drupal\gpt_translation\GPTTranslationItemInterface $job_item
   *   The job item entity.
   *
   * @see JobItem::getData()
   */
  public function getData(GPTTranslationItemInterface $job_item);

  /**
   * Saves a translation.
   *
   * @param \Drupal\gpt_translation\GPTTranslationItemInterface $job_item
   *   The job item entity.
   *
   * @return bool
   *   TRUE if the translation was saved successfully, FALSE otherwise.
   */
  public function saveTranslation(GPTTranslationItemInterface $job_item);

  /**
   * Return a title for this job item.
   *
   * @param string|false
   *   The label of the job item entity.
   */
  public function getLabel(GPTTranslationItemInterface $job_item);

  /**
   * Returns the Uri for this job item.
   *
   * @param \Drupal\gpt_translation\GPTTranslationItemInterface $job_item
   *   The job item entity.
   *
   * @return \Drupal\Core\Url|null
   *   The URL object for the source object.
   */
  public function getUrl(GPTTranslationItemInterface $job_item);

  /**
   * Returns an array of translatable source item types.
   */
  public function getItemTypes();

  /**
   * Returns the label of a source item type.
   *
   * @param $type
   *   The identifier of a source item type.
   */
  public function getItemTypeLabel($type);

  /**
   * Returns the type of a job item.
   *
   * @param \Drupal\gpt_translation\GPTTranslationItemInterface $job_item
   *   The job item.
   *
   * @return string
   *   A type that describes the job item.
   */
  public function getType(GPTTranslationItemInterface $job_item);

  /**
   * Gets language code of the job item source.
   *
   * @param \Drupal\gpt_translation\GPTTranslationItemInterface $job_item
   *   The job item.
   *
   * @return string|false
   *   Language code or FALSE for unknown.
   */
  public function getSourceLangCode(GPTTranslationItemInterface $job_item);

  /**
   * Gets existing translation language codes of the job item source.
   *
   * Returns language codes that can be used as the source language for a
   * translation job.
   *
   * @param \Drupal\gpt_translation\GPTTranslationItemInterface $job_item
   *   The job item.
   *
   * @return array
   *   Array of language codes.
   */
  public function getExistingLangCodes(GPTTranslationItemInterface $job_item);

}
