<?php

namespace Drupal\gpt_translation;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Interface for content entity source field processors.
 */
interface FieldProcessorInterface {

  /**
   * Extracts the translatatable data structure from the given field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field object.
   *
   * @return array $data
   *   An array of elements where each element has the following keys:
   *   - #text
   *   - #translate
   *
   * @see \Drupal\gpt_translation\Plugin\Source\ContentEntitySource::extractTranslatableData()
   */
  public function extractTranslatableData(FieldItemListInterface $field);

  /**
   * Process the translated data for this field back into a structure that can
   * be saved by the content entity.
   *
   * @param array $field_data
   *   The translated data for this field.
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field object.
   *
   * @see \Drupal\gpt_translation\Plugin\Source\ContentEntitySource::doSaveTranslations()
   */
  public function setTranslations($field_data, FieldItemListInterface $field);

}
