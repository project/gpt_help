<?php

namespace Drupal\gpt_translation;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for source ui controllers.
 */
interface SourcePluginUiInterface extends PluginInspectionInterface {

  /**
   * Form callback for the job item review form.
   */
  public function reviewForm(array $form, FormStateInterface $form_state, GPTTranslationItemInterface $item);

  /**
   * Form callback for the data item element form.
   */
  public function reviewDataItemElement(array $form, FormStateInterface $form_state, $data_item_key, $parent_key, array $data_item, GPTTranslationItemInterface $item);

  /**
   * Validation callback for the job item review form.
   */
  public function reviewFormValidate(array $form, FormStateInterface $form_state, GPTTranslationItemInterface $item);

  /**
   * Submit callback for the job item review form.
   */
  public function reviewFormSubmit(array $form, FormStateInterface $form_state, GPTTranslationItemInterface $item);

}
