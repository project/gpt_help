<?php

namespace Drupal\gpt_translation\Entity;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\SortArray;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Language\Language;
use Drupal\gpt_translation\GPTTranslationItemInterface;
use Drupal\Core\Render\Element;

/**
 * Entity class for the gpt_translation_item entity.
 *
 * @ContentEntityType(
 *   id = "gpt_translation_item",
 *   label = @Translation("GPT Translation Item"),
 *   module = "gpt_translation",
 *   handlers = {
 *     "form" = {
 *       "edit" = "Drupal\gpt_translation\Form\GPTTranslationItemForm",
 *       "abort" = "Drupal\gpt_translation\Form\GPTTranslationItemAbortForm",
 *     },
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *   },
 *   base_table = "gpt_translation_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/gpt_translation/items/{gpt_translation_item}",
 *   },
 * )
 */
class GPTTranslationItem extends ContentEntityBase implements GPTTranslationItemInterface {

  /**
   * Holds the unserialized source data.
   *
   * @var array
   */
  protected $unserializedData;

  /**
   * Statically cached state definitions.
   *
   * @var
   */
  protected static $stateDefinitions;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Item ID'))
      ->setDescription(t('The Job Item ID.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The GPT translation item UUID.'))
      ->setReadOnly(TRUE);

    $fields['plugin'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Plugin'))
      ->setDescription(t('The plugin of this GPT translation item.'))
      ->setSettings([
        'max_length' => 255,
      ]);

    $fields['item_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Item Type'))
      ->setDescription(t('The item type of this GPT translation item.'))
      ->setSettings([
        'max_length' => 255,
      ]);

    $fields['job_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Job Type'))
      ->setSettings(['max_length' => 255])
      ->setDefaultValue('translation');

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Item ID'))
      ->setDescription(t('The item ID of this GPT translation item.'))
      ->setCardinality(1);

    $fields['target_language'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Target Language'));

    $fields['data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Data'))
      ->setDescription(t('The source data'));

    $fields['extra_data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Extra Data'))
      ->setDescription(t('The extra data'));

    $states = static::getStates();
    $fields['state'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('GPT translation item state'))
      ->setDescription(t('The GPT translation item state'))
      ->setSetting('allowed_values', $states)
      ->setDefaultValue(static::STATE_INACTIVE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the item was last edited.'));
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $this->recalculateStatistics();
    if ($this->unserializedData) {
      $this->data = Json::encode($this->unserializedData);
    }
    elseif (empty($this->get('data')->value)) {
      $this->data = Json::encode([]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->get('plugin')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemType() {
    return $this->get('item_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemId() {
    $values = $this->get('entity_id')->getValue();
    return reset($values)['target_id'];
  }

  /**
   * Get entity.
   */
  public function getEntity() {
    $plugin = $this->getSourcePlugin();
    return $plugin->load($this->getItemType(), $this->getItemId());
  }

  /**
   * Get item target language.
   */
  public function getTargetLanguage() {
    return $this->get('target_language')->value;
  }

  /**
   * Get item target language name.
   */
  public function getTargetLanguageName() {
    $languages = \Drupal::languageManager()->getLanguages();
    if (isset($languages[$this->getTargetLanguage()])) {
      return $languages[$this->getTargetLanguage()]->getName();
    }
    return $this->getTargetLanguage();
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function label($langcode = NULL) {
    $label = $this->getSourceLabel();
    if ($label && strlen($label) > 128) {
      $label = Unicode::truncate($label, 128, TRUE);
    }
    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function addMessage($message, $variables = [], $type = 'status') {
    // Save the job item if it hasn't yet been saved.
    if (!$this->isNew() || $this->save()) {
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceLabel() {
    $entity = $this->getEntity();
    return $entity->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceUrl() {
    $entity = $this->getEntity();
    return $entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceType() {
    $entity = $this->getEntity();
    return $entity->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getData($key = [], $index = NULL) {
    $this->decodeData();
    if (empty($this->unserializedData)) {
      // Load the data from the source if it has not been set yet.
      $this->unserializedData = $this->getSourceData();
      $this->save();
    }
    if (empty($key)) {
      return $this->unserializedData;
    }
    if ($index) {
      $key = array_merge($key, [$index]);
    }
    return NestedArray::getValue($this->unserializedData, $key);
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceData() {
    if ($plugin = $this->getSourcePlugin()) {
      return $plugin->getData($this);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSourcePlugin() {
    if ($this->get('plugin')->value) {
      try {
        return \Drupal::service('plugin.manager.gpt_translation.source')
          ->createInstance($this->get('plugin')->value);
      } catch (PluginException $e) {
        // Ignore exceptions due to missing source plugins.
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function accepted($message = NULL, $variables = [], $type = 'status') {
    if (!isset($message)) {
      $source_url = $this->getSourceUrl();
      try {
        // @todo: Make sure we use the latest revision.
        //   Fix in https://www.drupal.org/project/tmgmt/issues/2979126.
        $translation = \Drupal::entityTypeManager()
          ->getStorage($this->getItemType())
          ->load($this->getItemId());
      } catch (PluginNotFoundException $e) {
        $translation = NULL;
      }
      if (isset($translation) && $translation->hasTranslation($this->getTargetLanguage())) {
        $translation = $translation->getTranslation($this->getTargetLanguage());
        try {
          $translation_url = $translation->toUrl();
        } catch (UndefinedLinkTemplateException $e) {
          $translation_url = NULL;
        }
        $message = $source_url && $translation_url ? 'The translation for <a href=":source_url">@source</a> has been accepted as <a href=":target_url">@target</a>.' : 'The translation for @source has been accepted as @target.';
        $variables = $source_url && $translation_url ? [
          ':source_url' => $source_url->toString(),
          '@source' => ($this->getSourceLabel()),
          ':target_url' => $translation_url->toString(),
          '@target' => $translation ? $translation->label() : $this->getSourceLabel(),
        ] : [
          '@source' => ($this->getSourceLabel()),
          '@target' => ($translation ? $translation->label() : $this->getSourceLabel()),
        ];
      }
      else {
        $message = $source_url ? 'The translation for <a href=":source_url">@source</a> has been accepted.' : 'The translation for @source has been accepted.';
        $variables = $source_url ? [
          ':source_url' => $source_url->toString(),
          '@source' => ($this->getSourceLabel()),
        ] : ['@source' => ($this->getSourceLabel())];
      }
    }
    $return = $this->setState(static::STATE_ACCEPTED, $message, $variables, $type);
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function active($message = NULL, $variables = [], $type = 'status') {
    if (!isset($message)) {
      $source_url = $this->getSourceUrl();
      $message = $source_url ? 'The translation for <a href=":source_url">@source</a> is now being processed.' : 'The translation for @source is now being processed.';
      $variables = $source_url ? [
        ':source_url' => $source_url->toString(),
        '@source' => ($this->getSourceLabel()),
      ] : ['@source' => ($this->getSourceLabel())];
    }
    return $this->setState(static::STATE_ACTIVE, $message, $variables, $type);
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state, $message = NULL, $variables = [], $type = 'debug') {
    // Return TRUE if the state could be set. Return FALSE otherwise.
    if (array_key_exists($state, GPTTranslationItem::getStates()) && $this->get('state')->value != $state) {
      $this->set('state', $state);
      // Changing the state resets the translator state.
      $this->save();
      // If a message is attached to this state change add it now.
      if (!empty($message)) {
        $this->addMessage($message, $variables, $type);
      }
    }
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isState($state) {
    return $this->getState() == $state;
  }

  /**
   * {@inheritdoc}
   */
  public function isAccepted() {
    return $this->isState(static::STATE_ACCEPTED);
  }

  /**
   *
   * @return boolean
   */
  public function isAbortable() {
    if ($this->isActive() || $this->isInQueue() || $this->isNeedsReview()) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->isState(static::STATE_ACTIVE);
  }

  /**
   * {@inheritdoc}
   */
  public function isInQueue() {
    return $this->isState(static::STATE_IN_QUEUE);
  }

  /**
   * {@inheritdoc}
   */
  public function isNeedsReview() {
    return $this->isState(static::STATE_REVIEW);
  }

  /**
   * {@inheritdoc}
   */
  public function isAborted() {
    return $this->isState(static::STATE_ABORTED);
  }

  /**
   * {@inheritdoc}
   */
  public function isInactive() {
    return $this->isState(static::STATE_INACTIVE);
  }

  /**
   * Recursively writes translated data to the data array of a job item.
   *
   * While doing this the #status of each data item is set to
   * TMGMT_DATA_ITEM_STATE_TRANSLATED.
   *
   * @param array $translation
   *   Nested array of translated data. Can either be a single text entry, the
   *   whole data structure or parts of it.
   * @param array|string $key
   *   (Optional) Either a flattened key (a 'key1][key2][key3' string) or a
   *   nested one, e.g. array('key1', 'key2', 'key2'). Defaults to an empty
   *   array which means that it will replace the whole translated data array.
   * @param int|null $status
   *   (Optional) The data item status that will be set. Defaults to NULL,
   *   which means that it will be set to translated unless it was previously
   *   set to preliminary, then it will keep that state.
   *   Explicitly pass TMGMT_DATA_ITEM_STATE_TRANSLATED,
   *   TMGMT_DATA_ITEM_STATE_PRELIMINARY or TMGMT_DATA_ITEM_STATE_REVIEWED to
   *   set it to that value. Other statuses are not supported.
   *
   */
  protected function addTranslatedDataRecursive(array $translation, $key = []) {
    if (isset($translation['#text'])) {
      $data_service = \Drupal::service('gpt_translation.data');
      $data = $this->getData($data_service->ensureArrayKey($key));
      // If we already have a translation text and it hasn't changed, don't update anything.
      if (!empty($data['#translation']['#text']) && $data['#translation']['#text'] == $translation['#text']) {
        return;
      }

      // In case the timestamp is not set consider it to be now.
      if (!isset($translation['#timestamp'])) {
        $translation['#timestamp'] = \Drupal::time()->getRequestTime();
      }
      // If we have a translation text and is different from new one create
      // revision.
      if (!empty($data['#translation']['#text']) && $data['#translation']['#text'] != $translation['#text']) {

        // Copy into $translation existing revisions.
        if (!empty($data['#translation']['#text_revisions'])) {
          $translation['#text_revisions'] = $data['#translation']['#text_revisions'];
        }

        $translation['#text_revisions'][] = [
          '#text' => $data['#translation']['#text'],
          '#origin' => $data['#translation']['#origin'] ?? 'remote',
          '#timestamp' => $data['#translation']['#timestamp'] ?? $this->getChangedTime(),
        ];
      }

      $values = [
        '#translation' => $translation,
        '#status' => 2,
      ];
      $this->updateData($key, $values);

      return;
    }

    foreach (Element::children($translation) as $item) {
      $this->addTranslatedDataRecursive($translation[$item], array_merge($key, [$item]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function dataItemRevert(array $key) {
    $data = $this->getData($key);
    if (!empty($data['#translation']['#text_revisions'])) {

      $prev_revision = end($data['#translation']['#text_revisions']);
      $data['#translation']['#text_revisions'][] = [
        '#text' => $data['#translation']['#text'],
        '#timestamp' => $data['#translation']['#timestamp'],
        '#origin' => $data['#translation']['#origin'],
      ];
      $data['#translation']['#text'] = $prev_revision['#text'];
      $data['#translation']['#origin'] = $prev_revision['#origin'];
      $data['#translation']['#timestamp'] = $prev_revision['#timestamp'];

      $this->updateData($key, $data);
      $this->addMessage('Translation for @key reverted to the latest version.', ['@key' => $key[0]]);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateData($key, $values = [], $replace = FALSE) {
    $this->decodeData();
    if ($replace) {
      NestedArray::setValue($this->unserializedData, \Drupal::service('gpt_translation.data')
        ->ensureArrayKey($key), $values);
    }
    foreach ($values as $index => $value) {
      // In order to preserve existing values, we can not aplly the values array
      // at once. We need to apply each containing value on its own.
      // If $value is an array we need to advance the hierarchy level.
      if (is_array($value)) {
        $this->updateData(array_merge(\Drupal::service('gpt_translation.data')
          ->ensureArrayKey($key), [$index]), $value);
      }
      // Apply the value.
      else {
        NestedArray::setValue($this->unserializedData, array_merge(\Drupal::service('gpt_translation.data')
          ->ensureArrayKey($key), [$index]), $value);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function resetData() {
    $this->data->value = NULL;
    $this->unserializedData = NULL;
    $this->getData();
  }

  /**
   * {@inheritdoc}
   */
  public function addTranslatedData(array $translation, $key = []) {

    if ($this->isInactive()) {
      // The job item can not be inactive and receive translations.
      $this->setState(self::STATE_ACTIVE);
    }

    $this->addTranslatedDataRecursive($translation, $key);
    // Check if the job item has all the translated data that it needs now.
    // Only attempt to change the status to needs review if it is currently
    // active.
    $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public function acceptTranslation() {
    if (!$this->isNeedsReview() || !$plugin = $this->getSourcePlugin()) {
      return FALSE;
    }
    if (!$plugin->saveTranslation($this)) {
      return FALSE;
    }
    // If the plugin could save the translation, we will set it
    // to the 'accepted' state.
    $this->accepted();
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function abortTranslation() {
    if (!$this->isAbortable()) {
      throw new \Exception('Cannot abort job item.');
    }
    $this->setState(self::STATE_ABORTED);
  }

  /**
   * {@inheritdoc}
   */
  public function getMessages($conditions = []) {
    return [];

    $query = \Drupal::entityQuery('tmgmt_message')
      ->accessCheck(TRUE)
      ->condition('tjiid', $this->id());
    foreach ($conditions as $key => $condition) {
      if (is_array($condition)) {
        $operator = isset($condition['operator']) ? $condition['operator'] : '=';
        $query->condition($key, $condition['value'], $operator);
      }
      else {
        $query->condition($key, $condition);
      }
    }
    $results = $query->execute();
    if (!empty($results)) {
      return Message::loadMultiple($results);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMessagesSince($time = NULL) {
    $time = isset($time) ? $time : \Drupal::time()->getRequestTime();
    $conditions = ['created' => ['value' => $time, 'operator' => '>=']];
    return $this->getMessages($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceLangCode() {
    return $this->getSourcePlugin()->getSourceLangCode($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getExistingLangCodes() {
    return $this->getSourcePlugin()->getExistingLangCodes($this);
  }

  /**
   * {@inheritdoc}
   */
  public function recalculateStatistics() {
    // Set translatable data from the current entity to calculate words.
    $this->decodeData();

    if (empty($this->unserializedData)) {
      $this->unserializedData = $this->getSourceData();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function language() {
    return new Language(['id' => Language::LANGCODE_NOT_SPECIFIED]);
  }

  /**
   * {@inheritdoc}
   */
  protected function invalidateTagsOnSave($update) {
    parent::invalidateTagsOnSave($update);
  }

  /**
   * {@inheritdoc}
   */
  public function getStateIcon() {
    $state_definitions = static::getStateDefinitions();

    $state_definition = NULL;
    if (isset($state_definitions[$this->getState()]['icon'])) {
      $state_definition = $state_definitions[$this->getState()];
    }

    if ($state_definition) {
      return [
        '#theme' => 'image',
        '#uri' => $state_definition['icon'],
        '#title' => $state_definition['label'],
        '#alt' => $state_definition['label'],
        '#width' => 16,
        '#height' => 16,
        '#weight' => $state_definition['weight'],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getStates() {
    return [
      static::STATE_ACTIVE => t('In progress'),
      static::STATE_IN_QUEUE => t('In queue'),
      static::STATE_REVIEW => t('Needs review'),
      static::STATE_ACCEPTED => t('Accepted'),
      static::STATE_ABORTED => t('Aborted'),
      static::STATE_INACTIVE => t('Inactive'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function getStateDefinitions() {
    if (!empty(static::$stateDefinitions)) {
      return static::$stateDefinitions;
    }

    static::$stateDefinitions = [
      static::STATE_IN_QUEUE => [
        'label' => t('In queue'),
        'type' => 'state',
        'icon' => \Drupal::service('extension.list.module')
            ->getPath('gpt_translation') . '/icons/hourglass.svg',
        'weight' => 0,
        'show_job_filter' => TRUE,
      ],
      static::STATE_REVIEW => [
        'label' => t('Needs review'),
        'type' => 'state',
        'icon' => \Drupal::service('extension.list.module')
            ->getPath('gpt_translation') . '/icons/ready.svg',
        'weight' => 5,
        'show_job_filter' => TRUE,
      ],
      static::STATE_ACCEPTED => [
        'label' => t('Accepted'),
        'type' => 'state',
        'weight' => 10,
      ],
      static::STATE_ABORTED => [
        'label' => t('Aborted'),
        'type' => 'state',
        'weight' => 15,
      ],
      static::STATE_INACTIVE => [
        'label' => t('Inactive'),
        'type' => 'state',
        'icon' => \Drupal::service('extension.list.module')
            ->getPath('gpt_translation') . '/icons/rejected.svg',
        'weight' => 20,
      ],
    ];

    \Drupal::moduleHandler()
      ->alter('gpt_translation_job_item_state_definitions', static::$stateDefinitions);

    foreach (static::$stateDefinitions as $key => &$definition) {
      // Ensure that all state definitions have a type and label key set.
      assert(!empty($definition['type']) && !empty($definition['label']), "State definition $key is missing label or type.");
      // Set the default weight to 25, after the default states.
      $definition += [
        'weight' => 25,
      ];
    }
    uasort(static::$stateDefinitions, [
      SortArray::class,
      'sortByWeightElement',
    ]);

    return static::$stateDefinitions;
  }

  /**
   * Ensures that the data is decoded.
   */
  protected function decodeData() {
    if (empty($this->unserializedData) && $this->get('data')->value) {
      $this->unserializedData = (array) Json::decode($this->get('data')->value);
    }
    if (!is_array($this->unserializedData)) {
      $this->unserializedData = [];
    }
  }

  /**
   * Get job type.
   *
   * @return mixed
   */
  public function getJobType() {
    return $this->get('job_type')->value;
  }

  /**
   * Get extra data.
   *
   * @return mixed
   */
  public function getExtraData() {
    $extra_data = $this->get('extra_data')->value;
    return Json::decode($extra_data);
  }

}
