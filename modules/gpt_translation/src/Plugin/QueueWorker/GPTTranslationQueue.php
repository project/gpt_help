<?php

namespace Drupal\gpt_translation\Plugin\QueueWorker;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\gpt_help\GPTApiService;
use Drupal\gpt_translation\GPTTranslate;
use Drupal\gpt_translation\GPTTranslationItemInterface;
use Drupal\gpt_help\Utility\StringHelper;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GPT Translation Queue.
 *
 * @QueueWorker(
 *   id = "gpt_translation_queue",
 *   title = @Translation("GPT Translation Queue"),
 *   cron = {"time" = 60}
 * )
 */
class GPTTranslationQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannel|\Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * GPT Api Service.
   *
   * @var \Drupal\gpt_help\GPTApiService
   */
  protected GPTApiService $GPTApiService;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * GPT translate.
   *
   * @var \Drupal\gpt_translation\GPTTranslate
   */
  protected $GPTTranslate;

  /**
   * Constructs.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   Email services.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger.
   */
  public function __construct(array                $configuration,
                                                   $plugin_id,
                                                   $plugin_definition,
                              LoggerChannelFactory $logger_factory,
                              EntityTypeManager    $entityTypeManager,
                              GPTApiService        $GPTApiService,
                              ConfigFactory        $config_factory,
                              GPTTranslate         $GPTTranslate) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger_factory->get('gpt_translation');
    $this->entityTypeManager = $entityTypeManager;
    $this->GPTApiService = $GPTApiService;
    $this->config = $config_factory->get('gpt_translation.settings');
    $this->GPTTranslate = $GPTTranslate;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('gpt_help.gpt_api'),
      $container->get('config.factory'),
      $container->get('gpt_translation.translate')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    $translation_item_id = $item->translation_item;
    /** @var \Drupal\gpt_translation\Entity\GPTTranslationItem $translation_item */
    $translation_item = $this->entityTypeManager->getStorage('gpt_translation_item')
      ->load($translation_item_id);
    $data = $translation_item->getData();
    $content = [];
    foreach ($data as $key => $value) {
      $item = \Drupal::service('gpt_translation.data')
        ->flatten($data[$key], $key);
      foreach ($item as $item_key => $item_value) {
        $exclude_flag = FALSE;
        $exclude_fields = ['target_id', 'target_revision_id', 'format', 'uri'];
        foreach ($exclude_fields as $exclude_field) {
          if (preg_match('/' . $exclude_field . '$/', $item_key)) {
            $exclude_flag = TRUE;
            break;
          }
        }
        if ($exclude_flag) {
          continue;
        }

        $origin_text = $item_value['#text'];
        $format = $item_value['#format'] ?? 'string';

        if ($translation_item->getJobType() == $translation_item::SEO_JOB_TYPE) {
          if ($format != 'full_html') {
            $content[] = StringHelper::prepareText($origin_text);
          }
          else {
            $origin_dom = StringHelper::getDOMDocument($origin_text);
            $content = array_merge($content, $this->getContentInDom($origin_dom));
          }
        }
        if ($translation_item->getJobType() == $translation_item::OPTIMIZE_JOB_TYPE) {
          $item_data = [
            '#text' => $origin_text,
            '#origin' => 'local',
          ];
          $translation_item->addTranslatedData($item_data, $item_key);
        }
        else {
          $lang_name = $translation_item->getTargetLanguageName();
          if ($format != 'full_html') {
            $origin_text = StringHelper::prepareText($origin_text);
            $text = $this->GPTTranslate->gptTranslateContent($origin_text, $lang_name);
          }
          else {
            $origin_dom = StringHelper::getDOMDocument($origin_text);
            $text = $this->GPTTranslate->handleFullHtmlField($origin_dom, $lang_name);
          }

          $item_data = [
            '#text' => $text,
            '#origin' => 'local',
          ];
          $translation_item->addTranslatedData($item_data, $item_key);
        }
      }
    }

    if ($translation_item->getJobType() == $translation_item::SEO_JOB_TYPE) {
      $content_string = implode(' ', $content);
      $content_string = Unicode::truncate($content_string, 3000, TRUE);
      $seo = $this->GPTTranslate->gptSeo($content_string);
      $seo_array = Json::decode($seo);
      if ($seo_array) {
        $translation_item->set('extra_data', $seo);
      }
    }

    $translation_item->setState(GPTTranslationItemInterface::STATE_REVIEW);
    $translation_item->save();
  }

  /**
   * Get content in dom.
   *
   * @param $dom
   * @param $content
   *
   * @return array
   */
  public function getContentInDom($dom, &$content = []) {
    foreach ($dom->childNodes as $node) {
      if ($node->nodeName == '#text') {
        $content[] = $node->nodeValue;
      }
      if ($node->hasChildNodes() && $node->nodeName != 'sup') {
        $this->getContentInDom($node, $content);
      }
    }
    return $content;
  }

}
