<?php

namespace Drupal\gpt_translation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\gpt_translation\Entity\GPTTranslationItem;
use Drupal\gpt_translation\Form\SourceOverviewForm;
use Drupal\gpt_translation\Plugin\Source\ContentEntitySource;

/**
 * Default ui controller class for source plugin.
 */
class SourcePluginUiBase extends PluginBase implements SourcePluginUiInterface {

  /**
   * {@inheritdoc}
   */
  public function reviewForm(array $form, FormStateInterface $form_state, GPTTranslationItemInterface $item) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function reviewDataItemElement(array $form, FormStateInterface $form_state, $data_item_key, $parent_key, array $data_item, GPTTranslationItemInterface $item) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function reviewFormValidate(array $form, FormStateInterface $form_state, GPTTranslationItemInterface $item) {
    // Nothing to do here by default.
  }

  /**
   * {@inheritdoc}
   */
  public function reviewFormSubmit(array $form, FormStateInterface $form_state, GPTTranslationItemInterface $item) {
    // Nothing to do here by default.
  }

  /**
   * Builds the overview form for the source entities.
   *
   * @param array $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $type
   *   Entity type.
   *
   * @return array
   *   Drupal form array.
   */
  public function overviewForm(array $form, FormStateInterface $form_state, $type) {
    $form += $this->overviewSearchFormPart($form, $form_state, $type);

    $form['#attached']['library'][] = 'gpt_translation/admin';

    $form['items'] = [
      '#type' => 'tableselect',
      '#header' => $this->overviewFormHeader($type),
      '#empty' => $this->t('No source items matching given criteria have been found.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function overviewFormValidate(array $form, FormStateInterface $form_state, $type) {
    // Nothing to do here by default.
  }

  /**
   * Submit handler for the source entities overview form.
   *
   * @param array $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $type
   *   Entity type.
   */
  public function overviewFormSubmit(array $form, FormStateInterface $form_state, $type) {
    // Handle search redirect.
    if ($this->overviewSearchFormRedirect($form, $form_state, $type)) {
      return;
    }

    $target_language = $form_state->getValue('target_language');

    $enforced_source_language = NULL;
    if ($form_state->getValue('source_language') != SourceOverviewForm::SOURCE) {
      $enforced_source_language = $form_state->getValue('source_language');
    }
    $seo_submit = $optimize_submit = FALSE;
    if ($form_state->getTriggeringElement()['#id'] == 'edit-seo-submit') {
      $seo_submit = TRUE;
    }
    elseif ($form_state->getTriggeringElement()['#id'] == 'edit-optimize-submit') {
      $optimize_submit = TRUE;
    }
    // Create gpt translation queue.
    $queue = \Drupal::service('queue')->get('gpt_translation_queue');
    $languages = \Drupal::languageManager()->getLanguages();
    foreach (array_filter($form_state->getValue('items')) as $item_id) {
      $entity = ContentEntitySource::load('node', $item_id);
      $source_lang = $entity->language()->getId();
      if ($target_language == $source_lang) {
        if (isset($languages[$target_language])) {
          $target_language_name = $languages[$target_language]->getName();
        }
        else {
          $target_language_name = $target_language;
        }
        $this->messenger()
          ->addMessage($this->t('@label can not be translated to @source_lang.',
            [
              '@label' => $entity->label(),
              '@source_lang' => $target_language_name,
            ]), 'warning');
        continue;
      }

      if ($seo_submit && !$entity->hasField('field_meta_tags')) {
        $this->messenger()
          ->addMessage($this->t('@label can not add seo.', ['@label' => $entity->label()]), 'warning');
        continue;
      }

      $data = [
        'entity_id' => $item_id,
        'target_language' => $target_language,
        'plugin' => 'content',
        'item_type' => 'node',
        'state' => GPTTranslationItemInterface::STATE_IN_QUEUE,
      ];
      if ($seo_submit) {
        $data['job_type'] = GPTTranslationItemInterface::SEO_JOB_TYPE;
      }
      elseif ($optimize_submit) {
        $data['job_type'] = GPTTranslationItemInterface::OPTIMIZE_JOB_TYPE;
      }

      $translation_item = GPTTranslationItem::create($data);
      $translation_item->save();

      $item = new \stdClass();
      $item->nid = $item_id;
      $item->target_language = $target_language;
      $item->translation_item = $translation_item->id();
      $item->seo_submit = $seo_submit;
      $queue->createItem($item);
    }

    $form_state->setRedirect('gpt_translation.source_overview_default');
    return TRUE;
  }

  /**
   * Builds search form for entity sources overview.
   *
   * @param array $form
   *   Drupal form array.
   * @param FormStateInterface $form_state
   *   Drupal form_state array.
   * @param string $type
   *   Entity type.
   *
   * @return array
   *   Drupal form array.
   */
  public function overviewSearchFormPart(array $form, FormStateInterface $form_state, $type) {
    // Add entity type and plugin_id value into form array
    // so that it is available in the form alter hook.
    $form_state->set('entity_type', $type);
    $form_state->set('plugin_id', $this->pluginId);

    // Add search form specific styling.
    $form['#attached']['library'][] = 'gpt_translation/source_search_form';

    $form['search_wrapper'] = [
      '#prefix' => '<div class="gpt_translation-sources-wrapper">',
      '#suffix' => '</div>',
      '#weight' => -15,
    ];
    $form['search_wrapper']['search'] = [
      '#tree' => TRUE,
    ];
    $form['search_wrapper']['search_submit'] = [
      '#type' => 'submit',
      '#value' => t('Search'),
      '#weight' => 90,
    ];
    $form['search_wrapper']['search_cancel'] = [
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * Gets languages form header.
   *
   * @return array
   *   Array with the languages for the header.
   */
  protected function getLanguageHeader() {
    $languages = [];
    foreach (\Drupal::languageManager()
               ->getLanguages() as $langcode => $language) {
      $languages['langcode-' . $langcode] = [
        'data' => $language->getName(),
      ];
    }

    return $languages;
  }

  /**
   * Performs redirect with search params appended to the uri.
   *
   * In case of triggering element is edit-search-submit it redirects to
   * current location with added query string containing submitted search form
   * values.
   *
   * @param array $form
   *   Drupal form array.
   * @param FormStateInterface $form_state
   *   Drupal form_state array.
   * @param $type
   *   Entity type.
   *
   * @return bool
   *   Returns TRUE, if redirect has been set.
   */
  public function overviewSearchFormRedirect(array $form, FormStateInterface $form_state, $type) {
    if ($form_state->getTriggeringElement()['#id'] == 'edit-search-cancel') {
      $form_state->setRedirect('gpt_translation.source_overview', [
        'plugin' => 'content',
        'item_type' => $type,
      ]);
      return TRUE;
    }
    elseif ($form_state->getTriggeringElement()['#id'] == 'edit-search-submit') {
      $query = [];

      foreach ($form_state->getValue('search') as $key => $value) {
        $query[$key] = $value;
      }
      $form_state->setRedirect('gpt_translation.source_overview', [
        'plugin' => 'content',
        'item_type' => $type,
      ], ['query' => $query]);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Builds the translation status render array with source and job item status.
   *
   * @param int $status
   *   The source status: original, missing, current or outofdate.
   * @param \Drupal\gpt_translation\GPTTranslationItemInterface|NULL $job_item
   *   The existing job item for the source.
   *
   * @return array
   *   The render array for displaying the status.
   */
  function buildTranslationStatus($status, GPTTranslationItemInterface $job_item = NULL) {
    if ($job_item && $job_item->getState() == GPTTranslationItemInterface::STATE_ACCEPTED) {
      $status = 'current';
    }
    elseif ($status != 'original') {
      $status = 'missing';
    }

    switch ($status) {
      case 'original':
        $label = t('Original language');
        $icon = 'core/misc/icons/bebebe/house.svg';
        break;

      case 'missing':
        $label = t('Not translated');
        $icon = 'core/misc/icons/bebebe/ex.svg';
        break;

      case 'outofdate':
        $label = t('Translation Outdated');
        $icon = \Drupal::service('extension.list.module')
            ->getPath('gpt_translation') . '/icons/outdated.svg';
        break;

      default:
        $label = t('Translation up to date');
        $icon = 'core/misc/icons/73b355/check.svg';
    }

    $build['source'] = [
      '#theme' => 'image',
      '#uri' => $icon,
      '#title' => $label,
      '#alt' => $label,
    ];

    // If we have an active job item, wrap it in a link.
    if ($job_item) {
      $url = $job_item->toUrl();
      $url->setOption('query', \Drupal::destination()->getAsArray());

      $item_icon = $job_item->getStateIcon();
      if ($item_icon) {
        $item_icon['#title'] = $this->t('Active job item: @state', ['@state' => $item_icon['#title']]);
        $build['job_item'] = [
          '#type' => 'link',
          '#url' => $url,
          '#title' => $item_icon,
        ];
      }
    }
    return $build;
  }

}
