<?php

namespace Drupal\gpt_translation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gpt_translation\GPTTranslationItemInterface;
use Drupal\workflows\Entity\Workflow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Function to create the ChatGPT Config Form.
 */
class GPTTranslationConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor for the class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'gpt_translation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gpt_translation_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gpt_translation.settings');

    $content_types = [];

    // Fetching all the node content types.
    $node_types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();

    foreach ($node_types as $type) {
      $content_types[$type->id()] = $type->label();
    }

    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Choose Content types for translation'),
      '#description' => $this->t('Please choose content types to enable gpt translation.'),
      '#options' => $content_types,
      '#default_value' => $config->get('content_types') ?? [],
      '#required' => TRUE,
    ];

    if (\Drupal::moduleHandler()->moduleExists('content_moderation')) {
      $form['workflows'] = [
        '#type' => 'details',
        '#title' => t('Default moderation states'),
        '#description' => t('Default moderation states for each workflow used in translations when Content Moderation is enabled.'),
        '#open' => TRUE,
      ];
      $form['workflows']['default_moderation_states'] = [
        '#tree' => TRUE,
      ];
      // Loop over all workflows.
      foreach (Workflow::loadMultipleByType('content_moderation') as $workflow) {
        $options = [];
        $states = $workflow->getTypePlugin()->getStates();
        foreach ($states as $state_id => $state) {
          $options[$state_id] = $state->label();
        }
        // Add a default moderation state select.
        $form['workflows']['default_moderation_states'][$workflow->id()] = [
          '#title' => t($workflow->label()),
          '#type' => 'select',
          '#options' => $options,
          '#default_value' => $config->get('default_moderation_states.' . $workflow->id()),
          '#empty_option' => '- Same as source -',
        ];
      }
    }

    $form['proper_nouns'] = [
      '#type' => 'textarea',
      '#default_value' => $config->get('proper_nouns'),
      '#title' => $this->t('Proper Nouns'),
      '#rows' => 4,
      '#description' => $this->t('Use JSON format:{"text1":"text1","text2":"text2"}'),
    ];

    $form['chunk_length'] = [
      '#type' => 'textfield',
      '#default_value' => $config->get('chunk_length') ?? 1000,
      '#title' => $this->t('Proper Nouns'),
    ];

    $optimize_types = GPTTranslationItemInterface::OPTIMIZE_TYPES;

    $form['optimize'] = [
      '#type' => 'details',
      '#title' => $this->t('Content Optimize'),
    ];

    foreach ($optimize_types as $optimize_type) {
      $label = str_replace('_', ' ', $optimize_type);
      $label = ucwords($label);
      $form['optimize'][$optimize_type] = [
        '#type' => 'details',
        '#title' => $this->t($label),
      ];
      $form['optimize'][$optimize_type][$optimize_type . '_system_prompt_text'] = [
        '#type' => 'textarea',
        '#rows' => 4,
        '#title' => $this->t('System prompt text'),
        '#default_value' => $config->get($optimize_type . '_system_prompt_text'),
      ];
      $form['optimize'][$optimize_type][$optimize_type . '_user_prompt_text'] = [
        '#type' => 'textarea',
        '#rows' => 4,
        '#title' => $this->t('User prompt text'),
        '#default_value' => $config->get($optimize_type . '_user_prompt_text'),
      ];
      $form['optimize'][$optimize_type][$optimize_type . '_full_html_user_prompt_text'] = [
        '#type' => 'textarea',
        '#rows' => 4,
        '#title' => $this->t('Full html user prompt text'),
        '#default_value' => $config->get($optimize_type . '_full_html_user_prompt_text'),
      ];
    }

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug model'),
      '#default_value' => $config->get('debug'),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('gpt_translation.settings')
      ->set('content_types', $form_state->getValue('content_types'))
      ->set('debug', $form_state->getValue('debug'))
      ->set('proper_nouns', $form_state->getValue('proper_nouns'))
      ->set('default_moderation_states', $form_state->getValue('default_moderation_states', []));

    $optimize_types = GPTTranslationItemInterface::OPTIMIZE_TYPES;
    foreach ($optimize_types as $optimize_type) {
      $config->set($optimize_type . '_system_prompt_text', $form_state->getValue($optimize_type . '_system_prompt_text'));
      $config->set($optimize_type . '_user_prompt_text', $form_state->getValue($optimize_type . '_user_prompt_text'));
      $config->set($optimize_type . '_full_html_user_prompt_text', $form_state->getValue($optimize_type . '_full_html_user_prompt_text'));
    }

    $config->save();
  }

}
