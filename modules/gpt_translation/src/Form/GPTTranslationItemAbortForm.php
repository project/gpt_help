<?php

namespace Drupal\gpt_translation\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting a node.
 */
class GPTTranslationItemAbortForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to abort the job item %title?', ['%title' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Aborted job items can no longer be accepted. The provider will not be notified.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\gpt_translation\Entity\GPTTranslationItem $entity */
    $entity = $this->entity;
    try {
      $entity->abortTranslation();
    } catch (\Exception $e) {
      $this->messenger()->addError(t('Job item cannot be aborted: %error.', [
        '%error' => $e->getMessage(),
      ]));
    }
    $form_state->setRedirectUrl($this->entity->toUrl());
  }

}
