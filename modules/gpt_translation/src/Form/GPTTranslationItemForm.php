<?php

namespace Drupal\gpt_translation\Form;

use Drupal\Component\Diff\Diff;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\filter\Entity\FilterFormat;
use \Drupal\Core\Diff\DiffFormatter;
use Drupal\gpt_help\Utility\StringHelper;
use Drupal\gpt_translation\Entity\GPTTranslationItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\gpt_translation\GPTTranslationItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the job item edit forms.
 *
 */
class GPTTranslationItemForm extends ContentEntityForm {

  /**
   * @var \Drupal\gpt_translation\Entity\GPTTranslationItem
   */
  protected $entity;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Source plugin manager.
   *
   * @var \Drupal\gpt_translation\SourceManager
   */
  protected $sourceManager;

  /**
   * Source plugin manager.
   *
   * @var \Drupal\gpt_translation\GPTTranslate
   */
  protected $GPTTranslate;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->dateFormatter = $container->get('date.formatter');
    $form->sourceManager = $container->get('plugin.manager.gpt_translation.source');
    $form->GPTTranslate = $container->get('gpt_translation.translate');
    return $form;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityForm::form().
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $item = $this->entity;
    if ($item->getJobType() == $item::SEO_JOB_TYPE) {
      $seo_data = $item->getExtraData();
      $entity = $item->getEntity();
      if ($entity->hasField('field_meta_tags')) {
        $meta_tags = $entity->get('field_meta_tags')->value;
        $meta_tags_array = unserialize($meta_tags);
      }
      $form['#title'] = $this->t('GPT SEO Item @source_label', ['@source_label' => $item->getSourceLabel()]);
      $form['review'] = ['#type' => 'container'];
      foreach (['title', 'description', 'keywords'] as $item) {
        $translation_default_value = $seo_data[$item] ?? '';
        if ($item == 'keywords') {
          $translation_default_value = is_array($translation_default_value) ? implode(', ', $seo_data['keywords']) : $translation_default_value;
        }
        $form['review'][$item] = [
          '#theme' => 'gpt_translation_data_items_form',
          '#ajaxid' => '',
          '#top_label' => $this->t('SEO @item', ['@item' => $item]),
          $item => [
            $item . '_value' => [
              '#tree' => TRUE,
              'source' => [
                '#type' => 'textarea',
                '#default_value' => $meta_tags_array[$item] ?? '',
                '#title' => $this->t('Source'),
                '#rows' => 3,
                '#disabled' => TRUE,
              ],
              'translation' => [
                '#type' => 'textarea',
                '#default_value' => $translation_default_value,
                '#title' => $this->t('GPT SEO'),
                '#rows' => 3,
                '#disabled' => FALSE,
              ],
            ],
          ],
        ];
      }
      return $form;
    }

    $form['#title'] = $this->t('GPT Translation Item @source_label', ['@source_label' => $item->getSourceLabel()]);

    $form['info'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['gpt_translation-ui-job-info', 'clearfix']],
      '#weight' => 0,
    ];

    $url = $item->getSourceUrl();
    $form['info']['source'] = [
      '#type' => 'item',
      '#title' => t('Source'),
      '#markup' => $url ? Link::fromTextAndUrl($item->getSourceLabel(), $url)
        ->toString() : $item->getSourceLabel(),
      '#prefix' => '<div class="gpt_translation-ui-source gpt_translation-ui-info-item">',
      '#suffix' => '</div>',
    ];

    $form['info']['sourcetype'] = [
      '#type' => 'item',
      '#title' => t('Source type'),
      '#markup' => $item->getSourceType(),
      '#prefix' => '<div class="gpt_translation-ui-source-type gpt_translation-ui-info-item">',
      '#suffix' => '</div>',
    ];


    $form['info']['target_language'] = [
      '#type' => 'item',
      '#title' => t('Target language'),
      '#markup' => $item->getTargetLanguageName(),
      '#prefix' => '<div class="gpt_translation-ui-target-language gpt_translation-ui-info-item">',
      '#suffix' => '</div>',
      '#value' => $item->getTargetLanguage(),
    ];

    $form['info']['changed'] = [
      '#type' => 'item',
      '#title' => t('Last change'),
      '#value' => $item->getChangedTime(),
      '#markup' => $this->dateFormatter->format($item->getChangedTime()),
      '#prefix' => '<div class="gpt_translation-ui-changed gpt_translation-ui-info-item">',
      '#suffix' => '</div>',
    ];
    $states = GPTTranslationItem::getStates();
    $form['info']['state'] = [
      '#type' => 'item',
      '#title' => t('State'),
      '#markup' => $states[$item->getState()],
      '#prefix' => '<div class="gpt_translation-ui-item-state gpt_translation-ui-info-item">',
      '#suffix' => '</div>',
      '#value' => $item->getState(),
    ];

    // Actually build the review form elements...
    $form['review'] = [
      '#type' => 'container',
    ];
    // Build the review form.
    $data = $item->getData();
    $this->trackChangedSource(\Drupal::service('gpt_translation.data')
      ->flatten($data), $form_state);
    $form_state->set('has_preliminary_items', FALSE);
    $form_state->set('all_preliminary', TRUE);
    // Need to keep the first hierarchy. So flatten must take place inside
    // of the foreach loop.
    foreach (Element::children($data) as $key) {
      $review_element = $this->reviewFormElement($form_state, \Drupal::service('gpt_translation.data')
        ->flatten($data[$key], $key), $key);
      if ($review_element) {
        $form['review'][$key] = $review_element;
      }
    }

    if ($form_state->get('has_preliminary_items')) {
      $form['translation_changes'] = [
        '#type' => 'container',
        '#markup' => $this->t('The translations below are in preliminary state and can not be changed.'),
        '#attributes' => [
          'class' => ['messages', 'messages--warning'],
        ],
        '#weight' => -50,
      ];
    }

    $form['#attached']['library'][] = 'gpt_translation/admin';
    $form['#attached']['library'][] = 'gpt_translation/gpt_translation_item_form';

    // The reject functionality has to be implement by the translator plugin as
    // that process is completely unique and custom for each translation service.

    // Give the source ui controller a chance to affect the review form.
    $source = $this->sourceManager->createUIInstance($item->getPlugin());
    $form = $source->reviewForm($form, $form_state, $item);
    return $form;
  }

  protected function actions(array $form, FormStateInterface $form_state) {
    $item = $this->entity;

    // Add the form actions as well.
    $actions['accept'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => t('Save as completed'),
      '#access' => $item->isNeedsReview() && \Drupal::currentUser()
          ->hasPermission('gpt translation save as completed'),
      '#validate' => ['::validateForm', '::validateGPTTranslationItem'],
      '#submit' => ['::submitForm', '::save'],
    ];
    $actions['save'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#access' => !$item->isAccepted() && !$form_state->get('all_preliminary'),
      '#submit' => ['::submitForm', '::save'],
    ];
    if ($item->isActive()) {
      $actions['save']['#button_type'] = 'primary';
    }
    if ($item->getJobType() == $item::SEO_JOB_TYPE) {
      $actions['accept']['#submit'] = $actions['save']['#submit'] = [
        '::submitForm',
        '::seoSave',
      ];
      $actions['accept']['#validate'] = ['::validateForm'];
    }

    $actions['abort_job_item'] = [
      '#type' => 'link',
      '#title' => t('Abort'),
      '#access' => $item->isAbortable(),
      '#url' => Url::fromRoute('entity.gpt_translation_item.abort_form', ['gpt_translation_item' => $item->id()]),
      '#weight' => 40,
      '#attributes' => [
        'class' => ['button', 'button--danger'],
      ],
    ];
    return $actions;
  }

  /**
   * Gets the translatable fields of a given job item.
   *
   * @param array $form
   *   The form array.
   *
   * @return array $fields
   *   Returns the translatable fields of the job item.
   */
  private function getTranslatableFields(array $form) {
    $fields = [];
    foreach (Element::children($form['review']) as $group_key) {
      foreach (Element::children($form['review'][$group_key]) as $parent_key) {
        foreach ($form['review'][$group_key][$parent_key] as $key => $data) {
          if (isset($data['translation'])) {
            $fields[$key] = [
              'parent_key' => $parent_key,
              'group_key' => $group_key,
              'data' => $data,
            ];
          }
        }
      }
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    /** @var GPTTranslationItem $item */
    $item = $this->buildEntity($form, $form_state);
    // First invoke the validation method on the source controller.
    $source_ui = $this->sourceManager->createUIInstance($item->getPlugin());
    $source_ui->reviewFormValidate($form, $form_state, $item);
  }

  /**
   * Form validate callback to validate the job item.
   */
  public function validateGPTTranslationItem(array &$form, FormStateInterface $form_state) {
    foreach ($this->getTranslatableFields($form) as $key => $value) {
      $parent_key = $value['parent_key'];
      $group_key = $value['group_key'];
      // If has HTML tags will be an array.
      if (isset($value['data']['translation']['value'])) {
        $translation_text = $value['data']['translation']['value']['#value'];
      }
      else {
        $translation_text = $value['data']['translation']['#value'];
      }

      // Validate that is not empty.
      if (empty($translation_text)) {
        $form_state->setError($form['review'][$group_key][$parent_key][$key]['translation'], $this->t('The field is empty.'));
        continue;
      }
    }
    if (!$form_state->hasAnyErrors()) {
      $this->messenger()->addStatus(t('Validation completed successfully.'));
    }
  }

  /**
   * Validate that the element is not longer than the max length.
   *
   * @param array $element
   *   The input element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateMaxLength(array $element, FormStateInterface &$form_state) {
    if (isset($element['#max_length'])
      && ($element['#max_length'] < strlen($element['#value']))) {
      $form_state->setError($element,
        $this->t('The field has @size characters while the limit is @limit.', [
          '@size' => strlen($element['#value']),
          '@limit' => $element['#max_length'],
        ])
      );
    }
  }

  /**
   * Overrides Drupal\Core\Entity\EntityForm::save().
   */
  public function save(array $form, FormStateInterface $form_state) {
    $item = $this->entity;
    // First invoke the submit method on the source controller.
    $source_ui = $this->sourceManager->createUIInstance($item->getPlugin());
    $source_ui->reviewFormSubmit($form, $form_state, $item);
    // Write changes back to item.
    $data_service = \Drupal::service('gpt_translation.data');
    foreach ($form_state->getValues() as $key => $value) {
      if (is_array($value) && isset($value['translation'])) {
        // Update the translation, this will only update the translation in case
        // it has changed. We have two different cases, the first is for nested
        // texts.
        $text = is_array($value['translation']) ? $value['translation']['value'] : $value['translation'];
        // Unmask the translation's HTML tags.
        $data_item = $item->getData($data_service->ensureArrayKey($key));
        $contexts = ['data_item' => $data_item, 'job_item' => $this->entity];
        \Drupal::moduleHandler()
          ->alter('gpt_translation_data_item_text_input', $text, $contexts);

        $data = [
          '#text' => $text,
          '#origin' => 'local',
        ];
        if ($data['#text'] == '' && $item->isActive() && $form_state->getTriggeringElement()['#value'] != '✓') {
          $data = NULL;
          continue;
        }
        $item->addTranslatedData($data, $key);
      }
    }
    // Check if the user clicked on 'Accept', 'Submit' or 'Reject'.
    if (!empty($form['actions']['accept']) && $form_state->getTriggeringElement()['#value'] == $form['actions']['accept']['#value']) {
      $item->acceptTranslation();
      // Print all messages that have been saved while accepting the reviewed
      // translation.
      foreach ($item->getMessagesSince() as $message) {
        // Ignore debug messages.
        if ($message->getType() == 'debug') {
          continue;
        }
        if ($text = $message->getMessage()) {
          $this->messenger()
            ->addMessage(new FormattableMarkup($text, []), $message->getType());
        }
      }
    }
    if ($form_state->getTriggeringElement()['#value'] == $form['actions']['save']['#value'] && isset($data)) {
      if ($item->getSourceUrl()) {
        $message = t('The translation for <a href=:job>@job_title</a> has been saved successfully.', [
          ':job' => $item->getSourceUrl()->toString(),
          '@job_title' => $item->label(),
        ]);
      }
      else {
        $message = t('The translation has been saved successfully.');
      }
      $this->messenger()->addStatus($message);
    }
    $item->save();
    $form_state->setRedirect('entity.gpt_translation_item.canonical', ['gpt_translation_item' => $item->id()]);
  }

  /**
   * Save seo data.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   *
   * @return void
   */
  public function seoSave(array $form, FormStateInterface $form_state) {
    $item = $this->entity;
    $seo = [
      'title' => $form_state->getValue(['title_value', 'translation']) ?? '',
      'description' => $form_state->getValue([
          'description_value',
          'translation',
        ]) ?? '',
      'keywords' => $form_state->getValue([
          'keywords_value',
          'translation',
        ]) ?? '',
    ];
    $seo_data = Json::encode($seo);
    $item->set('extra_data', $seo_data);
    if (!empty($form['actions']['accept']) && $form_state->getTriggeringElement()['#value'] == $form['actions']['accept']['#value']) {
      $entity = $item->getEntity();
      if ($entity->hasField('field_meta_tags')) {
        $meta_tags = $entity->get('field_meta_tags')->value;
        $meta_tags_array = unserialize($meta_tags);
        foreach ($seo as $key => $value) {
          if ($value) {
            $meta_tags_array[$key] = $value;
          }
        }
        $entity->set('field_meta_tags', serialize($meta_tags_array));
        $entity->save();
      }
      $item->set('state', GPTTranslationItem::STATE_ACCEPTED);
    }
    $item->save();
    $form_state->setRedirect('entity.gpt_translation_item.canonical', ['gpt_translation_item' => $item->id()]);
  }

  /**
   * Build form elements for the review form using flattened data items.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $data
   *   Flattened array of translation data items.
   * @param string $parent_key
   *   The key for $data.
   *
   * @return array|NULL
   *   Render array with the form element, or NULL if the text is not set.
   * @todo Mention in the api documentation that the char '|' is not allowed in
   * field names.
   *
   */
  function reviewFormElement(FormStateInterface $form_state, $data, $parent_key) {
    $review_element = NULL;

    foreach (Element::children($data) as $key) {
      $data_item = $data[$key];
      //$data_item['#status'] = $data[$key]['#status'] = 2;
      if (isset($data_item['#text']) && \Drupal::service('gpt_translation.data')
          ->filterData($data_item)) {
        // The char sequence '][' confuses the form API so we need to replace
        // it when using it for the form field name.
        $field_name = str_replace('][', '|', $key);

        // Ensure that the review form structure is initialized.
        $review_element['#theme'] = 'gpt_translation_data_items_form';
        $review_element['#ajaxid'] = $ajax_id = gpt_translation_review_form_element_ajaxid($parent_key);
        $review_element['#top_label'] = array_shift($data_item['#parent_label']);
        $leave_label = array_pop($data_item['#parent_label']);

        // Data items are grouped based on their key hierarchy, calculate the
        // group key and ensure that the group is initialized.
        $group_name = substr($field_name, 0, strrpos($field_name, '|'));
        if (empty($group_name)) {
          $group_name = '_none';
        }
        if (!isset($review_element[$group_name])) {
          $review_element[$group_name] = [
            '#group_label' => $data_item['#parent_label'],
          ];
        }

        // Initialize the form element for the given data item and make it
        // available as $element.
        $review_element[$group_name][$field_name] = [
          '#tree' => TRUE,
        ];
        $item_element = &$review_element[$group_name][$field_name];

        $item_element['label']['#markup'] = $leave_label;

        $form_state->set('all_preliminary', FALSE);

        $item_element['actions'] = [
          '#type' => 'container',
          '#access' => TRUE,
        ];

        $item = $this->entity;
        $job_type = $item->getJobType();
        if ($job_type == GPTTranslationItemInterface::OPTIMIZE_JOB_TYPE) {
          $item_element['actions'] = [
            '#type' => 'details',
            '#attributes' => ['class' => ['content-optimize-options']],
            '#title' => $this->t('Optimize'),
          ];
        }

        $item_element['below_actions'] = [
          '#type' => 'container',
        ];

        // Check if the field has a text format attached and check access.
        if (!empty($data_item['#format'])) {
          $format_id = $data_item['#format'];
          /** @var \Drupal\filter\Entity\FilterFormat $format */
          $format = FilterFormat::load($format_id);

          if (!$format || !$format->access('use')) {
            $item_element['actions']['#access'] = FALSE;
            $form_state->set('accept_item', FALSE);
          }
        }
        $item_element['actions'] += $this->buildActions($data_item, $key, $field_name, $ajax_id);

        // Manage the height of the textareas, depending on the length of the
        // description. The minimum number of rows is 3 and the maximum is 15.
        $rows = ceil(strlen($data_item['#text']) / 100);
        $rows = min($rows, 15);
        $rows = max($rows, 3);

        // Allow other modules to change the source and translation texts,
        // for example to mask HTML-tags.
        $source_text = $data_item['#text'];
        $translation_text = isset($data_item['#translation']['#text']) ? $data_item['#translation']['#text'] : '';
        $contexts = ['data_item' => $data_item, 'job_item' => $this->entity];
        \Drupal::moduleHandler()
          ->alter('gpt_translation_data_item_text_output', $source_text, $translation_text, $contexts);

        // Build source and translation areas.
        $item_element = $this->buildSource($item_element, $source_text, $data_item, $rows, $form_state);
        $item_element = $this->buildTranslation($item_element, $translation_text, $data_item, $rows, $form_state, FALSE);

        $item_element = $this->buildChangedSource($item_element, $form_state, $field_name, $key, $ajax_id);

        if (isset($form_state->get('validation_messages')[$field_name])) {
          $item_element['below']['validation'] = [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'gpt_translation_validation_message',
                'messages',
                'messages--warning',
              ],
            ],
            'message' => [
              '#markup' => Html::escape($form_state->get('validation_messages')[$field_name]),
            ],
          ];
        }
      }
    }
    return $review_element;
  }

  /**
   * Ajax callback for the job item review form.
   */
  function ajaxReviewForm(array $form, FormStateInterface $form_state) {
    $key = array_slice($form_state->getTriggeringElement()['#array_parents'], 0, 2);
    $render_data = NestedArray::getValue($form, $key);
    return $render_data;
  }

  /**
   * Submit handler for the HTML tag validation.
   */
  function validateTags(array $form, FormStateInterface $form_state) {
    $validation_messages = [];
    $field_count = 0;
    foreach ($form_state->getValues() as $field => $value) {
      if (is_array($value) && isset($value['translation'])) {
        if (!empty($value['translation'])) {
          $tags_validated = $this->compareHTMLTags($value['source'], $value['translation']);
          if ($tags_validated) {
            $validation_messages[$field] = $tags_validated;
            $field_count++;
          }
        }
      }
    }
    if ($field_count > 0) {
      $this->messenger()
        ->addError(t('HTML tag validation failed for @count field(s).', ['@count' => $field_count]));
    }
    else {
      $this->messenger()->addStatus(t('Validation completed successfully.'));
    }
    $form_state->set('validation_messages', $validation_messages);
    $request = \Drupal::request();
    $url = $this->entity->toUrl('canonical');
    if ($request->query->has('destination')) {
      $destination = $request->query->get('destination');
      $request->query->remove('destination');
      $url->setOption('query', ['destination' => $destination]);
    }
    $form_state->setRedirectUrl($url);
    $form_state->setRebuild();
  }

  /**
   * Submit rebuild.
   */
  function submitRebuild(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * Compare the HTML tags of source and translation.
   *
   * @param string $source
   *  Source text.
   * @param string $translation
   *  Translated text.
   */
  function compareHTMLTags($source, $translation) {
    $pattern = "/\<(.*?)\>/";
    if (is_array($source) && isset($source['value'])) {
      $source = $source['value'];
    }
    if (is_array($translation) && isset($translation['value'])) {
      $translation = $translation['value'];
    }
    preg_match_all($pattern, $source, $source_tags);
    preg_match_all($pattern, $translation, $translation_tags);
    $message = '';
    if ($source_tags != $translation_tags) {
      if (count($source_tags[0]) == count($translation_tags[0])) {
        $message .= 'Order of the HTML tags are incorrect. ';
      }
      else {
        $tags = implode(',', array_diff($source_tags[0], $translation_tags[0]));
        if (!empty($tags)) {
          $message .= 'Expected tags ' . $tags . ' not found. ';
        }
        $source_tags_count = $this->htmlTagCount($source_tags[0]);
        $translation_tags_count = $this->htmlTagCount($translation_tags[0]);
        $difference = array_diff_assoc($source_tags_count, $translation_tags_count);
        foreach ($difference as $tag => $count) {
          if (!isset($translation_tags_count[$tag])) {
            $translation_tags_count[$tag] = 0;
          }
          $message .= $tag . ' expected ' . $count . ', found ' . $translation_tags_count[$tag] . '.';
        }
        $unexpected_tags = array_diff_key($translation_tags_count, $source_tags_count);
        foreach ($unexpected_tags as $tag => $count) {
          if (!isset($translation_tags_count[$tag])) {
            $translation_tags_count[$tag] = 0;
          }
          $message .= $count . ' unexpected ' . $tag . ' tag(s), found.';
        }
      }

    }
    return $message;
  }

  /**
   * Compare the HTML tags of source and translation.
   *
   * @param array $tags
   *  array containing all the HTML tags.
   */
  function htmlTagCount($tags) {
    $counted_tags = [];
    foreach ($tags as $tag) {
      if (in_array($tag, array_keys($counted_tags))) {
        $counted_tags[$tag]++;
      }
      else {
        $counted_tags[$tag] = 1;
      }
    }
    return $counted_tags;
  }

  /**
   * Detect source changes and persist on $form_state.
   *
   * @param array $data
   *   The data items.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function trackChangedSource(array $data, FormStateInterface $form_state) {
    $item = $this->entity;
    $source_changed = [];
    $source_removed = [];
    foreach ($data as $key => $value) {
      if (is_array($value) && isset($value['#translate']) && $value['#translate']) {
        $key_array = \Drupal::service('gpt_translation.data')
          ->ensureArrayKey($key);
        try {
          $new_data = \Drupal::service('gpt_translation.data')
            ->flatten($item->getSourceData());
        } catch (\Exception $e) {
          $this->messenger()
            ->addError(t('The source does not exist any more.'));
          return;
        }
        $current_data = $item->getData($key_array);
        if (!isset($new_data[$key])) {
          $source_changed[$key] = t('This data item has been removed from the source.');
          $source_removed[$key] = TRUE;
        }
        elseif ($current_data['#text'] != $new_data[$key]['#text']) {
          $source_changed[$key] = t('The source has changed.');
        }
      }
    }
    $form_state->set('source_changed', $source_changed);
    $form_state->set('source_removed', $source_removed);
  }

  /**
   * Submit handler to show the diff table of the source.
   */
  public function showDiff(array $form, FormStateInterface $form_state) {
    $key = $form_state->getTriggeringElement()['#data_item_key'];
    $form_state->set('show_diff:' . $key, TRUE);
    $form_state->setRebuild();
  }

  /**
   * Submit handler to resolve the diff updating the Job Item source.
   */
  public function resolveDiff(array $form, FormStateInterface $form_state) {
    $item = $this->entity;
    $key = $form_state->getTriggeringElement()['#data_item_key'];
    $array_key = \Drupal::service('gpt_translation.data')->ensureArrayKey($key);
    $first_key = reset($array_key);
    $source_data = $item->getSourceData();
    $new_data = \Drupal::service('gpt_translation.data')
      ->flatten($source_data)[$key];
    $item->updateData($key, $new_data);
    if (isset($source_data[$first_key]['#label'])) {
      $item->addMessage('The conflict in the data item source "@data_item" has been resolved.', ['@data_item' => $source_data[$first_key]['#label']]);
    }
    else {
      $item->addMessage('The conflict in the data item source has been resolved.');
    }
    $item->save();
    $form_state->set('show_diff:' . $key, FALSE);
    $form_state->setRebuild();
  }

  /**
   * Builds the actions for a data item.
   *
   * @param array $data_item
   *   The data item.
   * @param string $key
   *   The data item key for the given structure.
   * @param string $field_name
   *   The name of the form element.
   * @param string $ajax_id
   *   The ID used for ajax replacements.
   *
   * @return array
   *   A list of action form elements.
   */
  protected function buildActions($data_item, $key, $field_name, $ajax_id) {
    /** @var \Drupal\gpt_translation\Entity\GPTTranslationItem $item */
    $item = $this->entity;
    $job_type = $item->getJobType();
    $actions = [];
    $actions['refresh'] = [
      '#access' => $job_type == GPTTranslationItemInterface::TRANSLATION_JOB_TYPE,
      '#type' => 'submit',
      '#value' => $this->t('Refresh'),
      '#attributes' => ['title' => t('Refresh')],
      '#name' => 'refresh-' . $field_name,
      '#limit_validation_errors' => [
        [$ajax_id],
        [$field_name],
      ],
      '#ajax' => [
        'method' => 'replace',
        'callback' => [$this, 'refreshTranslation'],
        'wrapper' => $ajax_id,
      ],
    ];

    if ($job_type == GPTTranslationItemInterface::TRANSLATION_JOB_TYPE) {
      return $actions;
    }

    $optimize_types = GPTTranslationItemInterface::OPTIMIZE_TYPES;
    foreach ($optimize_types as $optimize_type) {
      $label = str_replace('_', ' ', $optimize_type);
      $label = ucwords($label);
      $actions[$optimize_type] = [
        '#type' => 'submit',
        '#value' => $this->t($label),
        '#name' => $optimize_type . '-' . $field_name,
        '#limit_validation_errors' => [
          [$ajax_id],
          [$field_name],
        ],
        '#ajax' => [
          'method' => 'replace',
          'callback' => [$this, 'refreshTranslation'],
          'wrapper' => $ajax_id,
        ],
      ];
    }

    return $actions;
  }

  /**
   * Refresh get translation from gpt.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   *
   * @return AjaxResponse
   */
  public function refreshTranslation(array $form, FormStateInterface $form_state) {
    preg_match("/^(?P<action>[^-]+)-(?P<key>.+)/i", $form_state->getTriggeringElement()['#name'], $matches);

    $entity = $this->getEntity();
    $data_key = explode('|', $matches['key']);
    $data = $entity->getData($data_key);
    $origin_text = $data['#text'];
    $lang_name = $entity->getTargetLanguageName();

    $key = str_replace("|", "", $matches['key']);
    $key = str_replace("__", "-", $key);
    $key = str_replace("_", "-", $key);
    $key = 'edit-' . $key . '-translation';

    $response = new AjaxResponse();

    if (isset($data['#format']) && $data['#format'] == 'full_html') {
      $origin_dom = StringHelper::getDOMDocument($origin_text);
      if ($matches['action'] == 'refresh') {
        $text = $this->GPTTranslate->handleFullHtmlField($origin_dom, $lang_name);
      }
      else {
        $text = $this->GPTTranslate->optimizeFullHtmlField($origin_dom, $matches['action']);
      }
      $key = $key . '-value';
      $response->addCommand(
        new InvokeCommand(NULL, 'setCkeditorData', [
          $key,
          $text,
        ])
      );
    }
    else {
      if ($matches['action'] == 'refresh') {
        $text = $this->GPTTranslate->gptTranslateContent($origin_text, $lang_name);
      }
      else {
        $text = $this->GPTTranslate->gptOptimizeContent($origin_text, $matches['action']);
      }
      $key = '#' . $key;
      $response->addCommand(
        new InvokeCommand($key, 'val', [$text])
      );
    }
    return $response;
  }

  /**
   * Builds the notification and diff for source changes for a data item.
   *
   * @param array $item_element
   *   The form element for the data item.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $field_name
   *   The name of the form element.
   * @param string $key
   *   The data item key for the given structure.
   * @param string $ajax_id
   *   The ID used for ajax replacements.
   *
   * @return array
   *   The form element for the data item.
   */
  protected function buildChangedSource($item_element, FormStateInterface $form_state, $field_name, $key, $ajax_id) {
    // Check for source changes and offer actions.
    if (isset($form_state->get('source_changed')[$key])) {
      // Show diff if requested.
      if ($form_state->get('show_diff:' . $key)) {
        $keys = \Drupal::service('gpt_translation.data')
          ->ensureArrayKey($field_name);

        try {
          $new_data = \Drupal::service('gpt_translation.data')
            ->flatten($this->entity->getSourceData());
        } catch (\Exception $e) {
          $new_data = [];
        }

        $current_data = $this->entity->getData($keys);

        $diff_header = ['', t('Current text'), '', t('New text')];

        $current_lines = explode("\n", $current_data['#text']);
        $new_lines = explode("\n", isset($new_data[$key]) ? $new_data[$key]['#text'] : '');

        $diff_formatter = new DiffFormatter($this->configFactory());
        $diff = new Diff($current_lines, $new_lines);

        $diff_rows = $diff_formatter->format($diff);
        // Unset start block.
        unset($diff_rows[0]);

        $item_element['below']['source_changed']['diff'] = [
          '#type' => 'table',
          '#header' => $diff_header,
          '#rows' => $diff_rows,
          '#empty' => $this->t('No visible changes'),
          '#attributes' => [
            'class' => ['diff'],
          ],
        ];
        $item_element['below']['source_changed']['#attached']['library'][] = 'system/diff';
        $item_element['below_actions']['resolve-diff'] = [
          '#type' => 'submit',
          '#value' => t('Resolve'),
          '#attributes' => ['title' => t('Apply the changes of the source.')],
          '#name' => 'resolve-diff-' . $field_name,
          '#data_item_key' => $key,
          '#submit' => ['::resolveDiff'],
          '#ajax' => [
            'callback' => '::ajaxReviewForm',
            'wrapper' => $ajax_id,
          ],
        ];
      }
      else {
        $item_element['below']['source_changed'] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'gpt_translation_source_changed',
              'messages',
              'messages--warning',
            ],
          ],
        ];

        // Display changed message.
        $item_element['below']['source_changed']['message'] = [
          '#markup' => '<span>' . $form_state->get('source_changed')[$key] . '</span>',
          '#attributes' => ['class' => ['gpt_translation-review-message-inline']],
        ];

        if (!isset($form_state->get('source_removed')[$key])) {
          // Offer diff action.
          $item_element['below']['source_changed']['diff_button'] = [
            '#type' => 'submit',
            '#value' => t('Show change'),
            '#name' => 'diff-button-' . $field_name,
            '#data_item_key' => $key,
            '#submit' => ['::showDiff'],
            '#attributes' => ['class' => ['gpt_translation-review-message-inline']],
            '#ajax' => [
              'callback' => '::ajaxReviewForm',
              'wrapper' => $ajax_id,
            ],
          ];
        }
      }
    }
    return $item_element;
  }

  /**
   * Builds the translation form element for a data item.
   *
   * @param array $item_element
   *   The form element for the data item.
   * @param string $translation_text
   *   The translation's text to display in the item element.
   * @param array $data_item
   *   The data item.
   * @param int $rows
   *   The number of rows that should be displayed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param bool $is_preliminary
   *   TRUE is the data item is in the PRELIMINARY STATE, FALSE otherwise.
   *
   * @return array
   *   The form element for the data item.
   */
  protected function buildTranslation($item_element, $translation_text, $data_item, $rows, FormStateInterface $form_state, $is_preliminary) {
    if (!empty($data_item['#format']) && !$form_state->has('accept_item')) {
      $item_element['translation'] = [
        '#type' => 'text_format',
        '#default_value' => $translation_text,
        '#title' => t('Translation'),
        '#disabled' => $this->entity->isAccepted() || $is_preliminary,
        '#rows' => $rows,
        '#allowed_formats' => [$data_item['#format']],
      ];
    }
    elseif ($form_state->has('accept_item')) {
      $item_element['translation'] = [
        '#type' => 'textarea',
        '#title' => t('Translation'),
        '#value' => t('This field has been disabled because you do not have sufficient permissions to edit it. It is not possible to review or accept this job item.'),
        '#disabled' => TRUE,
        '#rows' => $rows,
      ];
    }
    else {
      $item_element['translation'] = [
        '#type' => 'textarea',
        '#default_value' => $translation_text,
        '#title' => t('Translation'),
        '#disabled' => $this->entity->isAccepted() || $is_preliminary,
        '#rows' => $rows,
      ];
      if (!empty($data_item['#max_length'])) {
        $item_element['translation']['#max_length'] = $data_item['#max_length'];
        $item_element['translation']['#element_validate'] = ['::validateMaxLength'];
      }
    }

    return $item_element;
  }

  /**
   * Builds the source form elements for a data item.
   *
   * @param array $item_element
   *   The form element for the data item.
   * @param string $source_text
   *   The source's text to display in the item element.
   * @param array $data_item
   *   The data item.
   * @param int $rows
   *   The number of rows that should be displayed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form element for the data item.
   */
  protected function buildSource($item_element, $source_text, $data_item, $rows, FormStateInterface $form_state) {
    if (!empty($data_item['#format']) && !$form_state->has('accept_item')) {
      $item_element['source'] = [
        '#type' => 'text_format',
        '#default_value' => $source_text,
        '#title' => t('Source'),
        '#disabled' => TRUE,
        '#rows' => $rows,
        '#allowed_formats' => [$data_item['#format']],
      ];
    }
    elseif ($form_state->has('accept_item')) {
      $item_element['source'] = [
        '#type' => 'textarea',
        '#title' => t('Source'),
        '#value' => t('This field has been disabled because you do not have sufficient permissions to edit it. It is not possible to review or accept this job item.'),
        '#disabled' => TRUE,
        '#rows' => $rows,
      ];
    }
    else {
      $item_element['source'] = [
        '#type' => 'textarea',
        '#default_value' => $source_text,
        '#title' => t('Source'),
        '#disabled' => TRUE,
        '#rows' => $rows,
      ];
    }
    return $item_element;
  }

}
