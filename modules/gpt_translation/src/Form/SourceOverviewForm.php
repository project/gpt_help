<?php

namespace Drupal\gpt_translation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\gpt_translation\GPTTranslationItemInterface;
use Drupal\gpt_translation\SourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Source overview form.
 */
class SourceOverviewForm extends FormBase {

  const ALL = '_all';

  const MULTIPLE = '_multiple';

  const SOURCE = '_source';

  /**
   * The source manager.
   *
   * @var \Drupal\gpt_translation\SourceManager
   */
  protected $sourceManager;

  /**
   * Constructs a new SourceLocalTasks object.
   *
   * @param \Drupal\gpt_translation\SourceManager $source_manager
   *   The source manager.
   */
  public function __construct(SourceManager $source_manager) {
    $this->sourceManager = $source_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.gpt_translation.source')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gpt_translation_overview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $plugin = NULL, $item_type = NULL) {
    $form['#title'] = $this->t('Content overview (Content Entity)');

    $form['operations'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['gpt_translation-source-operations-wrapper']],
    ];
    $form['operations']['checkout'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Checkout'),
      '#attributes' => ['class' => ['gpt_translation-source-checkout-wrapper']],
    ];

    $languages = gpt_translation_available_languages();

    $form['operations']['checkout']['target_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Target language'),
      '#empty_option' => $this->t('- Select later -'),
      '#empty_value' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      '#options' => $languages,
    ];

    $form['operations']['checkout']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#validate' => ['::validateItemsSelected', '::validateTranslated'],
      '#value' => $this->t('Request translation'),
      '#submit' => ['::submitForm'],
      '#attributes' => [
        'class' => ['request-button'],
      ],
    ];

    $form['operations']['checkout']['seo_submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#validate' => ['::validateItemsSelected'],
      '#value' => $this->t('Request SEO'),
      '#submit' => ['::submitForm'],
      '#attributes' => [
        'class' => ['request-button'],
      ],
    ];

    $form['operations']['checkout']['optimize_submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#validate' => ['::validateItemsSelected'],
      '#value' => $this->t('Content Optimize'),
      '#submit' => ['::submitForm'],
      '#attributes' => [
        'class' => ['request-button'],
      ],
    ];

    $form['legend'] = gpt_translation_color_legend();

    $plugin = 'content';
    $item_type = 'node';
    $form_state->set('plugin', $plugin);
    $form_state->set('item_type', $item_type);
    $source_ui = $this->sourceManager->createUIInstance($plugin);
    $form = $source_ui->overviewForm($form, $form_state, $item_type);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $plugin = $form_state->get('plugin');
    $item_type = $form_state->get('item_type');
    // Execute the validation method on the source plugin controller.
    $source_ui = $this->sourceManager->createUIInstance($plugin);
    $source_ui->overviewFormValidate($form, $form_state, $item_type);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plugin = $form_state->get('plugin');

    $item_type = $form_state->get('item_type');
    // Execute the submit method on the source plugin controller.
    $source_ui = $this->sourceManager->createUIInstance($plugin);
    $source_ui->overviewFormSubmit($form, $form_state, $item_type);
  }

  /**
   * {@inheritdoc}
   */
  public function sourceSelectSubmit(array &$form, FormStateInterface $form_state) {
    // Separate the plugin and source type.
    $source = $form_state->getValue('source');
    // Redirect to the selected source type.
    $form_state->setRedirectUrl($this->getUrlForSource($source));
  }

  /**
   * Submit method for Add to continuous jobs button.
   *
   * @param array $form
   *   Drupal form array.
   * @param FormStateInterface $form_state
   *   Drupal form_state array.
   */
  public function submitToContinuousJobs(array &$form, FormStateInterface $form_state) {
    $plugin = $form_state->get('plugin');
    $item_type = $form_state->get('item_type');
    // Execute the submit method on the source plugin controller.
    $source_ui = $this->sourceManager->createUIInstance($plugin);
    $source_ui->overviewSubmitToContinuousJobs($form_state, $item_type);
  }

  /**
   * AJAX callback to refresh form.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   *
   * @return array
   *   Form element to replace.
   */
  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $source = $form_state->getValue('source');

    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand($this->getUrlForSource($source)
      ->setAbsolute()
      ->toString()));
    return $response;
  }

  /**
   * Gets the url when selecting a source type.
   *
   * @param string $source
   *
   * @return \Drupal\Core\Url
   */
  public function getUrlForSource($source) {
    list($selected_plugin, $selected_item_type) = explode(':', $source);
    return Url::fromRoute('gpt_translation.source_overview', [
      'plugin' => $selected_plugin,
      'item_type' => $selected_item_type,
    ]);
  }

  /**
   * Validation for selected items.
   *
   * @param array $form
   *   An associate array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateItemsSelected(array $form, FormStateInterface $form_state) {
    $items = array_filter($form_state->getValue('items'));
    if (empty($items)) {
      $form_state->setError($form, t("You didn't select any source items."));
    }
  }

  /**
   * Validate for have been translated.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   *
   * @return void
   */
  public function validateTranslated(array $form, FormStateInterface $form_state) {
    $target_language = $form_state->getValue('target_language');
    if ($target_language == 'und') {
      $form_state->setError($form, t("Please select target language."));
    }
    $items = array_filter($form_state->getValue('items'));
    if ($items) {
      $query = \Drupal::database()->select('gpt_translation_item', 'gti');
      $result = $query->condition('gti.plugin', 'content')
        ->condition('gti.state', [GPTTranslationItemInterface::STATE_ABORTED], 'NOT IN')
        ->condition('gti.item_type', 'node')
        ->condition('gti.entity_id', $items, 'IN')
        ->condition('gti.job_type', 'translation')
        ->fields('gti', ['id'])
        ->orderBy('gti.changed', 'DESC')
        ->execute();
      $gpt_translation_items = $result->fetchAll();

      if ($gpt_translation_items) {
        $form_state->setError($form, $this->t("Some items you selected have been translated."));
      }
    }
  }

}

