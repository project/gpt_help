<?php

namespace Drupal\gpt_translation;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Default field processor.
 */
class LayoutSectionProcessor implements FieldProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function extractTranslatableData(FieldItemListInterface $field) {
    $data = [];
    $sections = $field->getSections();

    $data['#label'] = 'Layout Builder';

    foreach ($sections as $section_delta => $section) {
      /** @var \Drupal\layout_builder\Section $section */
      $components = $section->getComponents();

      $data[$section_delta] = [
        '#label' => $section->getLayoutId(),
      ];

      usort($components, "component_sorting");

      foreach ($components as $component) {
        /** @var \Drupal\layout_builder\SectionComponent $component */
        $component_config = $component->get('configuration');

        if (!isset($component_config['block_revision_id'])) {
          continue;
        }

        $component_weight = $component->getWeight();
        $data[$section_delta][$component_weight] = ['#label' => $component_config['label']];

        $block_content = \Drupal::entityTypeManager()
          ->getStorage('block_content')
          ->loadRevision($component_config['block_revision_id']);
        $block_fields = $block_content->getFields();

        $plugin = \Drupal::service('plugin.manager.gpt_translation.source')
          ->createInstance('content');
        if (isset($block_fields['body'])) {
          $block_content_data = $plugin->extractTranslatableData($block_content);
          $data[$section_delta][$component_weight] = $block_content_data;
        }

        foreach ($block_content->getFields() as $field) {
          if ($field->getFieldDefinition()->getType() != 'entity_reference'
            && $field->getFieldDefinition()
              ->getType() != 'entity_reference_revisions') {
            continue;
          }
          $paragraphs = $block_content->get($field->getName())
            ->referencedEntities();
          foreach ($paragraphs as $element_delta => $paragraph) {
            if ($paragraph instanceof Paragraph) {
              $p_data = $plugin->extractTranslatableData($paragraph);
              $data[$section_delta][$component_weight][$element_delta] = $p_data;
            }
          }
        }
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setTranslations($field_data, FieldItemListInterface $field) {
    $sections = $field->getSections();

    foreach ($sections as $section_delta => $section) {
      /** @var \Drupal\layout_builder\Section $section */
      $components = $section->getComponents();

      $data[$section_delta] = [
        '#label' => $section->getLayoutId(),
      ];

      foreach ($components as $component) {
        /** @var \Drupal\layout_builder\SectionComponent $component */
        $component_config = $component->get('configuration');
        $component_weight = $component->getWeight();

        if (isset($component_config['block_revision_id'])) {
          $block_content = \Drupal::entityTypeManager()
            ->getStorage('block_content')
            ->loadRevision($component_config['block_revision_id']);
          $block_fields = $block_content->getFields();

          $plugin = \Drupal::service('plugin.manager.gpt_translation.source')
            ->createInstance('content');
          if (isset($block_fields['body'])) {
            $plugin->doSaveTranslations($block_content, $field_data[$section_delta][$component_weight]);
            $block_plugin = $component->getPlugin();
            $block_plugin->setConfigurationValue('block_serialized', serialize($block_content));
            $configuration = $block_plugin->getConfiguration();
            $component->setConfiguration($configuration);
          }

          foreach ($block_content->getFields() as $field) {
            if ($field->getFieldDefinition()->getType() != 'entity_reference'
              && $field->getFieldDefinition()
                ->getType() != 'entity_reference_revisions') {
              continue;
            }
            $paragraphs = $block_content->get($field->getName())
              ->referencedEntities();
            foreach ($paragraphs as $element_delta => $paragraph) {
              if ($paragraph instanceof Paragraph) {
                $lasted_paragraph = $plugin->doSaveTranslations($paragraph, $field_data[$section_delta][$component_weight][$element_delta]);
                $block_plugin = $component->getPlugin();
                $block_content->set($field->getName(), [
                  'target_id' => $paragraph->id(),
                  'target_revision_id' => $lasted_paragraph->getRevisionId(),
                ]);
                $block_plugin->setConfigurationValue('block_serialized', serialize($block_content));
                $configuration = $block_plugin->getConfiguration();
                $component->setConfiguration($configuration);
              }
            }
          }

        }
      }
    }

  }

}
