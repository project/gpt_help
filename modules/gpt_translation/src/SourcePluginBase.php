<?php

namespace Drupal\gpt_translation;

use Drupal\Core\Plugin\PluginBase;

/**
 * Default controller class for source plugins.
 */
abstract class SourcePluginBase extends PluginBase implements SourcePluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getLabel(GPTTranslationItemInterface $job_item) {
    return t('@plugin item unavailable (@item)', [
      '@plugin' => $this->pluginDefinition['label'],
      '@item' => $job_item->getItemType() . ':' . $job_item->getItemId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(GPTTranslationItemInterface $job_item) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemTypes() {
    return isset($this->pluginDefinition['item types']) ? $this->pluginDefinition['item types'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getItemTypeLabel($type) {
    $types = $this->getItemTypes();
    if (isset($types[$type])) {
      return $types[$type];
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getType(GPTTranslationItemInterface $job_item) {
    return ucfirst($job_item->getItemType());
  }

  /**
   * {@inheritdoc}
   */
  public function getExistingLangCodes(GPTTranslationItemInterface $job_item) {
    return [$this->getSourceLangCode($job_item)];
  }

}

