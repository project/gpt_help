<?php

namespace Drupal\gpt_help;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Config\ConfigFactory;

/**
 * Service class to call OpenAI GPT APIs.
 */
class GPTApiService {

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannel|\Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The default http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructor of the class.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   An http client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Configuration factory.
   */
  public function __construct(ClientInterface $http_client, ConfigFactory $configFactory, LoggerChannelFactory $logger_factory) {
    $this->httpClient = $http_client;
    $this->configFactory = $configFactory;
    $this->logger = $logger_factory->get('gpt_help');
  }

  /**
   * Function to call the ChatGPT API.
   *
   * @param string $prompt_text
   *   Prompt text to feed the GPT API.
   *
   * @return string|GuzzleException
   *   GPT response message on success or error.
   */
  public function getGptResponse($prompt_text, $message = []) {
    $config = $this->configFactory->get('gpt_help.settings');
    $gpt_model_version = $config->get('gpt_model_version');
    $access_token = $config->get('chatgpt_token');
    $temperature = (int) $config->get('chatgpt_temperature');
    $max_tokens = (int) $config->get('chatgpt_max_token');

    if ($gpt_model_version == 'chatgpt') {
      // Preparing payload for GPT-3.5 or ChatGPT.
      $url = $config->get('chatgpt_endpoint');
      $model = $config->get('chatgpt_model');

      if (empty($message)) {
        $message[] = [
          "role" => "user",
          "content" => $prompt_text,
        ];
      }

      $payload = [
        "model" => $model,
        "messages" => $message,
        "temperature" => $temperature,
        "max_tokens" => $max_tokens,
      ];
    }
    elseif ($gpt_model_version == 'gpt4') {
      // Preparing payload for GPT-4.
      $url = $config->get('gpt4_endpoint');
      $model = $config->get('gpt4_model');
      $message[] = [
        "role" => "user",
        "content" => $prompt_text,
      ];

      $payload = [
        "model" => $model,
        "messages" => $message,
        "temperature" => $temperature,
        "max_tokens" => $max_tokens,
      ];
    }

    $header = [
      'Content-Type' => 'application/json',
      'Authorization' => 'Bearer ' . $access_token,
    ];
    $options = [
      'headers' => $header,
      'json' => $payload,
      'timeout' => $config->get('timeout') ?? 30,
    ];

    $max_attempts = 3;
    $backoff_factor = 1;
    $attempts = 0;
    $delay = 1;

    $text = '';
    while ($attempts < $max_attempts) {
      // Calling ChatGPT completion API.
      try {
        $response = $this->httpClient->request('POST', $url, $options);
        $result = $response->getBody()->getContents();
        $decoded_data = Json::decode($result);
        $text = $decoded_data['choices'][0]['message']['content'];
        break;
      }
      catch (\Exception $e) {
        $attempts++;
        if ($attempts >= $max_attempts) {
          throw $e;
        }
        sleep($delay);
        $delay *= $backoff_factor;
      }
    }

    // Processing success response data.
   if ($gpt_model_version == 'chatgpt' || $gpt_model_version == 'gpt4') {
      $text = $decoded_data['choices'][0]['message']['content'];
    }

    return $text;
  }

}
