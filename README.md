This module is created to utilize Chat GPT for translating the content of paragraphs embedded within inline blocks in the layout builder.

# Key features

1. Uses chat GPT to translate and replace texts, seamlessly integrating into the workflow.
2. Generate SEO information and optimize texts using chat GPT's functionality.

# Post-Installation

1. Configure GPT-related parameters at the URL: /admin/config/gpt.
2. Enable translation functionality for the desired content types at the URL: /admin/config/gpt_translation.
3. Request translations for specific articles at the URL: /admin/gpt_translation/sources.
4. Wait for cron to execute the requests in the queue.
5. Modify or save translated articles at the URL: /admin/gpt_translation/items/{item_id}.

# Similar projects

[Translation Management Tool module](https://www.drupal.org/project/tmgmt)

This module is inspired by Translation Management Tool module. It has removed most of the workflow features from that module and utilizes replacing the original text instead of adding translations.
